# README

This is a Blog application written while following the TreeHouse Ruby on Rails track.

There are a few minor differences to the official project, mostly just edits for either Rails Ver. compatibility changes, or just personal stylings.

You can find the learning material I followed here: https://teamtreehouse.com/tracks/rails-development